'use strict';

const {app, BrowserWindow} = require('electron');
const url = require('url');
const path = require('path');

require('electron-reload')(__dirname);

let win = null;

function boot(){
	win = new BrowserWindow({
		title: "Cheater",
		width: 1000,
		height: 700,
		minHeight: 550,
  		minWidth: 700,
		frame: false
	})
	win.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
      	protocol: 'file:',
		slashes: true
	}))
	win.on('closed', () => {
		win = null;
	})
}
app.on('ready', boot);
app.on('window-all-closed', () => {
  app.quit();
});
