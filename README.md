<p align="center"><img src="https://www.nitwel.de/images/Cheater.png" width="200px">
<p>
<h1 align="center">Cheater</h1>
<h3>Version 1.0.6</h3>
<p>
Cheater is an advanced compare tool, to find structural simularities between two texts.
The way the algorythm compares both texts is oriented on the way, humans do compare things, to create a more natural but still accurate result.
<p>
Being still in development, Cheater is already functional, tho some features like the import and export of texts are not finished yet.
<p>
Download the application: <a href="https://www.nitwel.de/projects/Cheater">click here</a>

Developed by Nils Twelker
<a href="https://www.nitwel.de">nitwel.de</a>