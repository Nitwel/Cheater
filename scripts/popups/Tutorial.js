class Tutorial {

	constructor() {

	}

	clicked(event) {
		var pages = document.getElementById("info-page").children;
		for (var i = 0; i < pages.length; i++) {
			if(pages[i].contains(event.target)) {
				this.openPage(pages[i].getAttribute("tut"));
			}
		}

	}

	openPage(nr) {
		var pages = document.getElementById("info-page").children;
		for (var i = 0; i < pages.length; i++) {
			if(nr == i){
				pages[i].classList.add("info-page-selected");
			} else {
				pages[i].classList.remove("info-page-selected");
			}
		}
		document.getElementById("info-box").style.marginLeft = (nr * -100 + "%");
	}
}