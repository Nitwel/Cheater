class LanguageSelect {

	constructor() {
		this.element = document.querySelector("#lang-select span");
		this.elements = document.querySelectorAll("#lang-select ul li");
	}

	clicked(event) {
		this.element.innerHTML = event.selectedTarget.innerHTML;
		main.settings.setLanguage(event.selectedTarget.getAttribute("value"));
	}


}