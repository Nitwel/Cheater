class SettingsPopup {

	constructor() {
		this.colorSelect = new ColorSelect();
		this.fontSizeSelect = new FontSizeSelect();
		this.languageSelect = new LanguageSelect();
	}

	clicked(event) {
		var settings = main.settings;

		if(document.getElementById("color-select").contains(event.target)){
			this.colorSelect.clicked(event);
		}

		document.querySelectorAll("#lang-select ul li").forEach((element) => {
			if(element.contains(event.target)){
				event.selectedTarget = element;
				this.languageSelect.clicked(event);
			}
		});
	}
}
		