class ColorSelect {
	constructor() {
		this.elements = document.getElementById("color-select").children;
	}

	clicked(event) {
		for (var i = 0; i < this.elements.length; i++) {
			
			var element = this.elements[i];

			if(event.target == element || element.contains(event.target)) {

				var targetID = element.getAttribute("color");

				main.settings.setColor(targetID);
				this.updateElements(targetID);
			}
		}
	}

	updateElements(targetID) {
		for (var i = 0; i < this.elements.length; i++) {
			var element = this.elements[i];

			if(targetID == i){
				element.classList.add("color-selected");
			} else {
				element.classList.remove("color-selected");
			}
		}
	}
}