class FontSizeSelect {
	constructor() {
		this.element = document.getElementById("settings-fontSize-input");
		this.display = document.getElementById("settings-fontSize-number");
		this.element.addEventListener("input", (event) => {this.moved(event)});
	}

	moved(event) {
		var size = event.target.value;
		main.settings.setFontSize(size);
		this.display.innerHTML = size;
	}
}