class CircleStats {

	constructor() {
		this.circleNumber = document.getElementById("stats-percentage-number");
		this.circle = document.getElementById("stats-circle");
	}

	setCircleTo(percent) {
	    var circumference = 989.60;
	    var percentage = percent * circumference / 100;
	    this.circle.style.strokeDasharray = (percentage+' '+circumference);
	}

	setText(text){
		this.circleNumber.innerHTML = text;
	}

	animatePercentage() {
		var repo = Repository.getInstance();
		var elements = document.getElementsByClassName("count");
		
		for (var i = 0; i < elements.length; i++) {

			var animateElement = function(element) {
				var number = element.innerHTML;
				var count = 1;
				var easeout = function(percent, max) {
					var x = (percent/100);
					return (x*(2-x) * max);
				}
				var easein = function(percent, max) {
					var x = (percent/100);
					return (x*x*x*x*x*x * max);
				}

				var animate = () => {
					var val = easeout(count, number);
					element.innerHTML = repo.roundInt(val, 0);
					count++;
					if(count <= 100){
						setTimeout(() => {
							animate();
						}, Math.abs(easein(count, 2000) - easein(count-1, 2000)));
						
					}
				}
				animate();
			}
			animateElement(elements[i]);
			
		}
	}
}