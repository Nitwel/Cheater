class PopupManager {

	constructor(popupHeight = 530, popupWidth = 700) {
		this.open = false;
		this.height = popupHeight;
		this.width = popupWidth;

		this.settingsPopup = new SettingsPopup();
		this.tutorial = new Tutorial();

		this.popupBackground = document.getElementById("popup-background");
		this.popupFrame = document.getElementById("popup-frame");
	}

	clicked(event) {

		//close Popup
		if(this.popupBackground == event.target || document.getElementById("popup-close").contains(event.target)){
			this.closePopup();
		} else
		//
		if(document.getElementById("settings").contains(event.target)){
			this.settingsPopup.clicked(event);
		} else
		//open Info Popup on already open Popup
		if(document.getElementById("tutorial-start").contains(event.target)){
			setTimeout(() => {
				this.openPopup("info",800,600);
			}, 200);
		} else
		//calculate simularity
		if(document.getElementById("stats-circle-ripple").contains(event.target)){
			main.comparator.calculatePercentage();
		} else
		if(document.getElementById("about-link").contains(event.target)){
			main.shell.openExternal('https://nitwel-web.de');
		}
		if(document.getElementById("info-page").contains(event.target)){
			this.tutorial.clicked(event);
		}
	}

	openPopup(popuptype, width, height) {
		if(this.open){
			this.closePopup();
			setTimeout(() => {
				this.openPopup(popuptype, width, height);
			}, 200);
		} else {
			this.height = height;
			this.width = width;
			this.open = true;
			var popup = document.getElementById(popuptype);

			this.popupFrame.style.width = width+"px";
			this.popupFrame.style.height = height+"px";
			popup.style.display = "block";
			this.popupBackground.style.display = "inline-block";
			setTimeout(() => {
				this.popupBackground.style.opacity = "1";
			}, 1);

		}
	}

	closePopup() {

		this.popupFrame.style.animation = "scaleout 0.2s";
		this.popupBackground.style.opacity = "0";

		setTimeout(() => {
			var popups = document.getElementsByClassName("popup");
			for (var i = 0; i < popups.length; i++) {
				popups[i].style.display = "none";
			}

			this.popupFrame.style.animation = "";
			this.popupBackground.style.display = "none";

			this.open = false;
		}, 200);
	}
}