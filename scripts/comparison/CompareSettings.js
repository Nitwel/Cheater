class CompareSettings {

	constructor() {
		this.tool = document.getElementById("compare-mode");
		this.compareModeSentence = document.getElementById("compare-mode-sentence");
		this.compareModeWord = document.getElementById("compare-mode-word");
		document.getElementById("compare-mode-scala").addEventListener("input", (event) => {
			document.getElementById("compare-mode-value").innerHTML = event.target.value;
			this.setSimularMarked(event.target.value/100);
		});
	}

	setVisible(visible) {
		if(visible)
			this.tool.classList.add("compare-show");
		else
			this.tool.classList.remove("compare-show");
	}

	setCompareMode(mode) {
		if(mode == "word") {
			this.compareModeSentence.classList.remove("compare-active");
			this.compareModeWord.classList.add("compare-active");
		} else if( mode == "sentence") {
			this.compareModeSentence.classList.add("compare-active");
			this.compareModeWord.classList.remove("compare-active");
		}
	}

	clicked(event) {
		var comparator = main.comparator;
		if(document.getElementById("compare-mode-sentence").contains(event.target)) {
			comparator.setCompareMode("sentence");
			this.setCompareMode("sentence");
			
		} else
		if(document.getElementById("compare-mode-word").contains(event.target)) {
			comparator.setCompareMode("word");
			this.setCompareMode("word");
			
		}
	}

	setSimularMarked(percent = null) {
		if(!percent) {
			percent = document.getElementById("compare-mode-scala").value/100;
		}
		var elements = elements = document.querySelectorAll("*[sim]");
		
		for (var i = 0; i < elements.length; i++) {
			if(elements[i].getAttribute("sim") >= percent){
				elements[i].classList.add("marked");
			} else {
				elements[i].classList.remove("marked");
			}		
		}
	}

}