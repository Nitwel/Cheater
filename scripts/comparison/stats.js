function Sentence(words, connection, simularity){
	this.words = words;
	this.simularity = simularity;
	this.connection = connection;
}
function Word(word, connection, simularity){
	this.word = word;
	this.connection = connection;
	this.simularity = simularity;
}

function Connection(simularity){
	this.connection = null;
	this.simularity = simularity;
}

onmessage = function(e) {
	calculateStats(e.data.data.text1, e.data.data.text2);
}

var textConnections = null;

function calculateStats(text1, text2) {

	var firstTextResult = cutText(text1);
	var firstText = firstTextResult[0];
	var firstTextSpacer = firstTextResult[1];

	var secondTextResult = cutText(text2);
	var secondText = secondTextResult[0];
	var secondTextSpacer = secondTextResult[1];

	var minSimularity = 0.75;

	var textSimularity = 0;
	var totalSentences = 0;
	var totalWords = 0;
	var sameSentences = 0;
	var sameWords = 0;

	textConnections = calcSimular(firstText, secondText);

	for (var i = 0; i < textConnections.length; i++) {
		findBestConn(i, textConnections);
	}



	for (var i1 = 0; i1 < textConnections.length; i1++) {

		var connIndex = -1;

		for (var i2 = 0; i2 < textConnections[i1].length; i2++) {

			if(textConnections[i1][i2].connection == i1){
				connIndex = i2;
			}
		}
		if(connIndex != -1){
			var simularity = textConnections[i1][connIndex].simularity;

			firstText[i1].simularity = simularity;
			firstText[i1].connection = connIndex;

			secondText[connIndex].simularity = simularity;
			secondText[connIndex].connection = i1;
			connectWords(firstText[i1], secondText[connIndex]);

			textSimularity += simularity;
			if(simularity > minSimularity) {
				sameSentences++;
			}
			
		}
	}
	for (var i = 0; i < firstText.length; i++) {

		totalWords += firstText[i].words.length;

		var words = firstText[i].words;
		for (var w = 0; w < words.length; w++) {
			if(words[w].connection != null){
				if(words[w].simularity > minSimularity){
					sameWords++;
				}
			}
		}
	}

	for (var i = 0; i < secondText.length; i++) {
		totalWords += secondText[i].words.length;

		var words = secondText[i].words;
		for (var w = 0; w < words.length; w++) {
			if(words[w].connection != null){
				if(words[w].simularity > minSimularity){
					sameWords++;
				}
			}
		}
	}
	totalWords /= 2;
	sameWords /= 2;

	totalSentences = (firstText.length + secondText.length)/2;

	textSimularity = textSimularity/totalSentences;

	self.postMessage({
		"done": {'simularity': textSimularity, totalSentences, totalWords, sameSentences, sameWords, 'data':[firstText, secondText], 'spacer':[firstTextSpacer,secondTextSpacer]}
	});

}

function findBestConn(id, connections) {
	var maxIndex = -1;
	var simularity = 0;
	var localConnections = connections[id];

	for (var i = 0; i < localConnections.length; i++) {
		
		var localConnection = localConnections[i];

		if(localConnection.simularity > simularity){

			var usedConn = localConnection.connection;

			if(usedConn != null){

				if(localConnection.simularity > connections[usedConn][i].simularity) {

					maxIndex = i;
					simularity = localConnection.simularity;
				}
			} else {

				maxIndex = i;
				simularity = localConnection.simularity;
			}
		}
	}
	if(maxIndex != -1){
		var oldConn = localConnections[maxIndex].connection;

		//updating the connections for all other words
		for (var i = 0; i < connections.length; i++) {
			connections[i][maxIndex].connection = id;
		}
		if(oldConn != null){
			findBestConn(oldConn, connections);
		}
	}
}

function calcSimular(text1, text2){
	var connections = [];

	for (var i = 0; i < text1.length; i++) {
		connections[i] = new Array();
	}

	for (var i1 = 0; i1 < text1.length; i1++) {

		for (var i2 = 0; i2 < text2.length; i2++) {

			var diff = Math.abs(i1 - i2);
			var simular = sentenceEquals(text1[i1], text2[i2]);///Math.sqrt(diff+1);
			
			connections[i1][i2] = new Connection(simular);
		}
	}
	return connections;
}

function cutText(text) {
	var spacers = [];
	var text = text.split("");
	var sentnences = [];
	var words = [];
	var wordChace = [];

	for (var c = 0; c < text.length; c++) {

		var char = text[c];
		var charBefore = (c == 0) ? null: text[c-1];
		var charAfter = (c == text.length-1) ? null: text[c+1];

		if(char == '\n'){
			
			spacers.push(sentnences.length-1);

		} else if( (isNewSentence(char) && !isNumber(charBefore)) || charAfter == "\n"){
			
			if(wordChace.replace(" ","") != ""){
				words.push(new Word(wordChace, null, null));
				wordChace = "";
			}

			if(words.length != 0){
				sentnences.push(new Sentence(words, null, null));
				words = [];
			}
		} else if(char == ' ') {
			if(wordChace.replace(" ","") != ""){
				words.push(new Word(wordChace, null, null));
				wordChace = "";
			}
		} else {
			wordChace += char;
		}	
	}


	if(wordChace != ""){
		words.push(new Word(wordChace, null, null));
		sentnences.push(new Sentence(words, null, null));
	}

	return [sentnences, spacers];
}

function sentenceEquals(sentence1, sentence2) {
	var words1 = sentence1.words;
	var words2 = sentence2.words;
	var sentenceSimularity = 0;
	
	var connections = [];

	for (var i = 0; i < words1.length; i++) {
		connections[i] = new Array();
	}

	for (var i1 = 0; i1 < words1.length; i1++) {
		for (var i2 = 0; i2 < words2.length; i2++) {
			var simularity = wordsEquals(words1[i1].word, words2[i2].word);
			connections[i1][i2] = new Connection(simularity);
		}
	}

	for (var i = 0; i < connections.length; i++) {
		findBestConn(i, connections);
	}

	for (var i1 = 0; i1 < connections.length; i1++) {

		var connFound = false;

		for (var i2 = 0; i2 < connections[i1].length && !connFound; i2++) {
			var conn = connections[i1][i2];
			if( connFound = conn.connection == i1){
				sentenceSimularity += conn.simularity;
			}
		}

	}
	return sentenceSimularity/((words1.length + words2.length)/2);
}

function connectWords(sentence1, sentence2){
	var words1 = sentence1.words;
	var words2 = sentence2.words;
	var sentenceSimularity = 0;
	
	var connections = [];

	for (var i = 0; i < words1.length; i++) {
		connections[i] = new Array();
	}

	for (var i1 = 0; i1 < words1.length; i1++) {
		for (var i2 = 0; i2 < words2.length; i2++) {
			var simularity = wordsEquals(words1[i1].word, words2[i2].word);
			connections[i1][i2] = new Connection(simularity);
		}
	}

	for (var i = 0; i < connections.length; i++) {
		findBestConn(i, connections);
	}

	for (var i1 = 0; i1 < connections.length; i1++) {

		for (var i2 = 0; i2 < connections[i1].length; i2++) {
			var conn = connections[i1][i2];
			if(conn.connection == i1){
				
				words1[i1].simularity = conn.simularity;
				words1[i1].connection = i2;

				words2[i2].simularity = conn.simularity;
				words2[i2].connection = i1;
			}
		}

	}
}

function wordsEquals(word1, word2) {
	if(word1.toLowerCase() == word2.toLowerCase()){
		return 1;
	}
	//aufteilen der Wörter in Buchstaben
	chars1 = word1.toLowerCase().split("");
	chars2 = word2.toLowerCase().split("");

	var connections = [];
	var simularity = 0;

	for (var i1 = 0; i1 < word1.length; i1++) {

		var contains = -1;
		var difference = -1;

		for (var i2 = 0; i2 < word2.length; i2++) {

			if(!connections.includes(i2) && chars1[i1] == chars2[i2]){
				var diff = Math.abs(i1 - i2);

				if(diff < difference || difference == -1){
					contains = i2;
					difference = diff;
				}
			}
			
		}
		if(contains != -1){
			connections.push(contains);
			simularity += 1/(difference+1);
		}
	}
	return simularity / ((chars1.length + chars2.length)/2);
}

function compareValues(simularity, diff) {
	return Math.pow(simularity, (diff+11)/10);
}

function isNewSentence(c) {
	return c == '.' || c == '!' || c == '?';
}

function isNumber(n){
	return n == '0' || n == '1' || n == '2' || n == '3' || n == '4'|| n == '5' || n == '6' || n == '7' || n == '8'|| n == '9';
}
