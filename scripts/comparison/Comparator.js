class Comparator {

	constructor(remote) {
		this.window = remote.getCurrentWindow();
		this.textCalculated = false;
		this.compareMode = "off";
		this.repo = Repository.getInstance();
		this.textData = null;
		this.stats = new Worker("scripts/comparison/stats.js");
		this.circleStats = new CircleStats();
		this.circleStats.setCircleTo(0);
		this.hoverTool = new HoverTool();
	}

	calculatePercentage() {

		if(this.textCalculated){
			return;
		}

		var textField1 = document.getElementById("text-surce");
		var text1 = textField1.value;
		var textField2 = document.getElementById("text-clone");
		var text2 = textField2.value;

		if(!text1|| !text2)return;

		var loader = document.getElementById("stats-loader");
		var timeout = setTimeout(() => {
			loader.style.display = "block";
		}, 1000);

		this.stats.postMessage({
			'data': {'text1': text1, 'text2': text2 }
		});

		this.stats.onmessage = (event) => {
			if(event.data.status != null){

				this.window.setProgressBar(event.data.status);
			} else
			if(event.data.done != null){

				clearTimeout(timeout);
				loader.style.display = "none";

				this.textData = event.data.done;
				//setting stats

				this.setStats();
				this.window.setProgressBar(0);

				this.renderText();

				this.compareText();
				this.setCompareMode("sentence");
				main.compareSettings.setSimularMarked();
			}

		}
	}

	setStats() {
		document.getElementById("stats-percentage-calc").style.display = "none";

		var displayValue = document.getElementById("stats-percentage-number");
		displayValue.style.display = "block";
		displayValue.innerHTML = this.repo.toPercent(this.textData.simularity);
		document.getElementById("stats-percentage-indicator").style.display = "inline-block";

		var stats = document.querySelectorAll(".stats-section p");

		var repo = Repository.getInstance();
		for (var i = 0; i < stats.length; i++) {
			switch(i) {
				case 0: stats[i].innerHTML = repo.roundInt(this.textData.totalSentences,2);
					break;
				case 1: stats[i].innerHTML = repo.roundInt(this.textData.totalWords,2);
					break;
				case 2: stats[i].innerHTML = repo.roundInt(this.textData.sameSentences,2);
					break;
				case 3: stats[i].innerHTML = repo.roundInt(this.textData.sameWords,2);
					break;
				default: break;
			}
		}

		this.circleStats.setCircleTo(this.repo.toPercent(this.textData.simularity));
		this.circleStats.animatePercentage(this.repo.toPercent(this.textData.simularity));

		main.sidebar.setToolActive("edit", false);
		main.sidebar.setToolActive("compare", true);
		main.sidebar.setToolDisabled("compare", false);
		main.compareSettings.setVisible(true);
		this.textCalculated = true;
	}

	resetStats() {

		document.getElementById("text-content").removeEventListener("keydown", this.keyCheck)

		document.getElementById("stats-percentage-calc").style.display = "";
		document.getElementById("stats-percentage-number").style.display = "";
		document.getElementById("stats-percentage-indicator").style.display = "";

		var stats = document.querySelectorAll(".stats-section p");
		for (var i = 0; i < stats.length; i++) {
			switch(i) {
				case 0: stats[i].innerHTML = 0;
					break;
				case 1: stats[i].innerHTML = 0;
					break;
				case 2: stats[i].innerHTML = 0;
					break;
				case 3: stats[i].innerHTML = 0;
					break;
				default: break;
			}
		}
		this.circleStats.setCircleTo(0);

		main.sidebar.setToolActive("edit", true);
		main.sidebar.setToolActive("compare", false);
		main.sidebar.setToolDisabled("compare", true);
		main.compareSettings.setVisible(false);

		document.getElementById("text-surce-calc").innerHTML = "";
		document.getElementById("text-clone-calc").innerHTML = "";

		this.textCalculated = false;
	}

	renderText() {

		for (var t = 0; t <= 1; t++) {
			var target = (t == 0)?document.getElementById("text-surce-calc") : document.getElementById("text-clone-calc");
			var scopeId = Math.abs(t-1);
			var sentences = this.textData.data[t];

			for (var s = 0; s < sentences.length; s++) {

				var sentConn = sentences[s].connection;
				var sentence = document.createElement("span");

				sentence.id = (t + "." + s);
				if(sentConn != null) {
					sentence.setAttribute("sim", this.repo.roundInt(sentences[s].simularity,2));
					sentence.setAttribute("sentence", scopeId + "." + sentConn);
					sentence.classList.add("sentence-marked");
				}
				
				var words = sentences[s].words;
				for (var w = 0; w < words.length; w++) {

					var wordConn = words[w].connection;

					var word = document.createElement("span");
					if(wordConn != null){
						word.classList.add("word-marked");
						word.setAttribute("sim", this.repo.roundInt(words[w].simularity, 2));
						word.setAttribute("word", scopeId + "." + sentConn + "." + wordConn);
					}
					word.id = (t+"."+s+"."+w);
					word.innerHTML = words[w].word;

					sentence.appendChild(word);

					if(w != words.length -1)
						sentence.innerHTML += " ";

				}
				target.appendChild(sentence);
				sentence.innerHTML += ".";
				target.innerHTML += " ";

				var spacer = this.textData.spacer[t];
				for (var i = 0; i < spacer.length; i++) {
					if(spacer[i] == s){
						target.innerHTML += "<br>";
					}
				}	
			}
			var endSpacer = document.createElement("div");
			endSpacer.style.width = "100%";
			endSpacer.style.height = "50vh";
			target.appendChild(endSpacer);
			
		}
	}

	editText() {
		if(this.textCalculated) {
			document.getElementById("text-content").classList.remove("compare");
		}

		main.sidebar.setToolActive("edit", true);
		main.sidebar.setToolActive("compare", false);
		main.compareSettings.setVisible(false);

		document.getElementById("text-content").addEventListener("keydown", this.keyCheck = (event) => {
			this.resetStats();
		});
	}

	compareText() {
		if(this.textCalculated){

			document.getElementById("text-content").classList.add("compare");

			main.sidebar.setToolActive("edit", false);
			main.sidebar.setToolActive("compare", true);
			main.compareSettings.setVisible(true);
			main.compareSettings.setCompareMode("sentence");
			this.setCompareMode("sentence");

		}
	}

	mouseenter(event) {
		var target = event.realTarget;
		target.style.backgroundColor = "var(--text-marked-dark)";
		var connection = target.getAttribute((this.compareMode=="word")?"word":"sentence");
		document.getElementById(connection).style.backgroundColor = "var(--text-marked-dark)";
		this.hoverTool.setVisible(true);
		this.hoverTool.setSimularity(target.getAttribute("sim")*100);
	}

	mouseleave(event) {
		var target = event.realTarget;
		target.style.backgroundColor = "";
		var connection = target.getAttribute((this.compareMode=="word")?"word":"sentence");
		document.getElementById(connection).style.backgroundColor = "";
		this.hoverTool.setVisible(false);
	}

	mousemove(event) {
		this.hoverTool.updatePosition(event.pageX, event.pageY);
		var repo = Repository.getInstance();

		if(this.compareMode == "word") {
			var newTarget = repo.getParentWithClass(event.target, "word-marked");
			if(this.oldTarget != newTarget){

				if(this.oldTarget) {

					event.realTarget = this.oldTarget;
					this.mouseleave(event);
				}
				if(newTarget){

					event.realTarget = newTarget;
					this.mouseenter(event);

				}

				this.oldTarget = newTarget;
			}

		} else if(this.compareMode == "sentence") {
			var newTarget = repo.getParentWithClass(event.target, "sentence-marked");
			if(this.oldTarget != newTarget){

				if(this.oldTarget) {

					event.realTarget = this.oldTarget;
					this.mouseleave(event);
				}
				if(newTarget){
					event.realTarget = newTarget;
					this.mouseenter(event);

				} 

				this.oldTarget = newTarget;
			}
		}

		return;
	}

	setCompareMode(mode) {
		var textContent = document.getElementById("text-content");
		if(mode == "word") {
			textContent.classList.add("markWords");
			textContent.classList.remove("markSentences");
			this.compareMode = mode;
		} else if (mode == "sentence") {
			textContent.classList.remove("markWords");
			textContent.classList.add("markSentences");
			this.compareMode = mode;
		} else {
			textContent.classList.remove("markWords");
			textContent.classList.remove("markSentences");
			this.compareMode = "off";
		}
	}
}