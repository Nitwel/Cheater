class HoverTool {
	constructor() {
		this.visible = false;
		this.tool = document.getElementById("hover-tool");
	}

	setVisible(visible) {
		clearTimeout(this.timeout);
		this.visible = visible;
		if(visible) {
			this.tool.style.display = "block";
			this.timeout = setTimeout(() => {
				this.tool.style.opacity = "1";
			},1);
		} else {
			this.tool.style.opacity = "0";
			this.timeout = setTimeout(() => {
				this.tool.style.display = "";
			},200);
		}
	}

	setSimularity(number) {
		this.tool.innerHTML = "Ähnlichkeit: "+Math.round(number);
	}

	updatePosition(x,y) {
		this.tool.style.left = x - 75;
		this.tool.style.top = y - 40;

	}
}