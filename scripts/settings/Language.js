class Language {

	constructor(language = "de") {
		this.language = language;
		this.setLanguage(language);
	}

	getLanguage() {
		return this.language;
	}

	setLanguage(language) {
		this.language = language;
		var request = new XMLHttpRequest();
		
		request.onreadystatechange = function() {
      		if(this.readyState == 4 && this.status == 200){
        		if(this.responseText != ""){
	          		var data = JSON.parse(this.responseText);

	          		var lang = document.querySelectorAll("[lang]");
	          		for (var i = 0; i < lang.length; i++) {
	          			var depth = lang[i].getAttribute("lang").split("-");

	          			var text = data[depth[0]];
	          			for (var d = 1; d < depth.length; d++) {
	          				text = text[depth[d]];
	          			}

	          			lang[i].innerHTML = text;
	          		}
        		}
      		}
    	}

    	request.open("GET", "lang/"+language+".json");
		request.send();
	}

}