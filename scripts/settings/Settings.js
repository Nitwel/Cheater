class Settings {

	constructor() {
		this.language = new Language();
	}

	setFontSize(size) {
		this.fontSize = size;
		document.querySelector("html").style.setProperty("--text-size",size+"px");
	}

	setLanguage(lang) {
		this.language.setLanguage(lang);
	}

	setColor(color) {
		var s = document.querySelector("html");
		switch(color) {
			case "0": //blue
				s.style.setProperty("--main-color", "#1976d2"); //700
				s.style.setProperty("--main-color-light", "#63a4ff"); //700light
				s.style.setProperty("--main-color-dark", "#004ba0"); //700dark
				s.style.setProperty("--main-color-click", "#1E88E5"); //600
				s.style.setProperty("--main-color--dark-click", "#1565C0"); //800

				s.style.setProperty("--text-marked", "#90CAF9"); //200
				s.style.setProperty("--text-marked-light", "#BBDEFB"); //100
				s.style.setProperty("--text-marked-dark", "#64B5F6"); //300

				s.style.setProperty("--icon-color-active", "#1976d2"); //700
				break;

			case "1": //green
				s.style.setProperty("--main-color", "#388E3C"); //700
				s.style.setProperty("--main-color-light", "#6abf69"); //700light
				s.style.setProperty("--main-color-dark", "#00600f"); //700dark
				s.style.setProperty("--main-color-click", "#43A047"); //600
				s.style.setProperty("--main-color--dark-click", "#2E7D32"); //800
				
				s.style.setProperty("--text-marked", "#A5D6A7"); //200
				s.style.setProperty("--text-marked-light", "#C8E6C9"); //100
				s.style.setProperty("--text-marked-dark", "#81C784"); //300

				s.style.setProperty("--icon-color-active", "#388E3C"); //700
				break;
			case "2": //red
				s.style.setProperty("--main-color", "#D32F2F"); //700
				s.style.setProperty("--main-color-light", "#ff6659"); //700light
				s.style.setProperty("--main-color-dark", "#9a0007"); //700dark
				s.style.setProperty("--main-color-click", "#E53935"); //600
				s.style.setProperty("--main-color--dark-click", "#C62828"); //800
				
				s.style.setProperty("--text-marked", "#EF9A9A"); //200
				s.style.setProperty("--text-marked-light", "#FFCDD2"); //100
				s.style.setProperty("--text-marked-dark", "#E57373"); //300

				s.style.setProperty("--icon-color-active", "#D32F2F"); //700
				break;
			case "3": //yellow
				s.style.setProperty("--main-color", "#FBC02D"); //700
				s.style.setProperty("--main-color-light", "#fff263"); //700light
				s.style.setProperty("--main-color-dark", "#c49000"); //700dark
				s.style.setProperty("--main-color-click", "#FDD835"); //600
				s.style.setProperty("--main-color--dark-click", "#F9A825"); //800
				
				s.style.setProperty("--text-marked", "#FFF59D"); //200
				s.style.setProperty("--text-marked-light", "#FFF9C4"); //100
				s.style.setProperty("--text-marked-dark", "#FFF176"); //300

				s.style.setProperty("--icon-color-active", "#FBC02D"); //700
				break;
			case "4": //orange
				s.style.setProperty("--main-color", "#F57C00"); //700
				s.style.setProperty("--main-color-light", "#ffad42"); //700light
				s.style.setProperty("--main-color-dark", "#bb4d00"); //700dark
				s.style.setProperty("--main-color-click", "#FB8C00"); //600
				s.style.setProperty("--main-color--dark-click", "#EF6C00"); //800
				
				s.style.setProperty("--text-marked", "#FFCC80"); //200
				s.style.setProperty("--text-marked-light", "#FFE0B2"); //100
				s.style.setProperty("--text-marked-dark", "#FFB74D"); //300

				s.style.setProperty("--icon-color-active", "#F57C00"); //700
				break;
			default: break;
		}
	}
}

// $("#compare-mode-scala").on("input", function() {
// 	var value = $("#compare-mode-scala").val();
// 	$("#compare-mode-value").text(value+" %");
// });

