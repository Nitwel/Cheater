class Main {

	constructor() {
		var {remote,shell} = require('electron');

		this.remote = remote;
		this.shell = shell;
		this.sidebar = new Sidebar();
		this.rippleEffect = new RippleEffect();
		this.dropdowns = new Dropdowns();
		this.settings = new Settings();
		this.comparator = new Comparator(remote);
		this.compareSettings = new CompareSettings();
		this.popupManager = new PopupManager();

		this.textContent = document.getElementById("text-content");

		document.addEventListener("click", (event) => {this.clicked(event)});
		document.addEventListener("resize", (event) => {this.resized(event)});
		document.addEventListener("mousemove", (event) => {this.mousemove(event)});
	}

	clicked(event) {

		if(document.getElementById("sidebar-container").contains(event.target)){
			this.sidebar.clicked(event);
		} else
		if(document.getElementById("popup-background").contains(event.target)){
			this.popupManager.clicked(event);
		} else
		if(document.getElementById("compare-mode").contains(event.target)) {
			this.compareSettings.clicked(event);
		} else 		
		if(document.getElementById('scale-window').contains(event.target)) {
			this.scale();
		} else
		if(document.getElementById('minimize-window').contains(event.target)) {
			this.minimize();
		} else
		if(document.getElementById('close-window').contains(event.target)) {
			this.close();
		}

		document.querySelectorAll(".dropdown").forEach((dropdown) => {
			if(dropdown.contains(event.target)){
				event.dropdownTarget = dropdown;
				this.dropdowns.clicked(event);
			}
		});
	}

	resized(event) {
		scalePopup();
	}

	mousemove(event) {
		if(this.textContent.contains(event.target) && this.comparator.compareMode != "off"){
			this.comparator.mousemove(event);
		}
	}

	close() {
		var window = this.remote.getCurrentWindow();
		window.close();
	}

	minimize() {
		var window = this.remote.getCurrentWindow();
		window.minimize();
	}

	scale() {
		var window = this.remote.getCurrentWindow();
		if(window.isMaximized()){
			window.unmaximize();
		} else {
			window.maximize();
		}
	}
}