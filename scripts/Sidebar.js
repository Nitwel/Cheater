class Sidebar {

	constructor() {
		this.open = false;
		this.sidebar = document.getElementById('sidebar-mover');
		this.sidebarToggle = document.getElementById("sidebar-toggle");

		this.editTool = document.getElementById("edit-tool");
		this.compareTool = document.getElementById("compare-tool");
		this.statsTool = document.getElementById("stats-tool");
		this.settingsTool = document.getElementById("settings-tool");
		this.aboutTool = document.getElementById("about-tool");
		this.setToolDisabled("import", true);
		this.setToolDisabled("export", true);

	}

	clicked(event){
		if(this.sidebarToggle.contains(event.target)) {
			this.toggleSidebar();
		} else {
			this.closeSidebar();
		}

		var popupManager = main.popupManager;

		if(this.statsTool.contains(event.target)){

			popupManager.openPopup("stats",700,530);

		} else if(this.settingsTool.contains(event.target)){

			popupManager.openPopup("settings",700,530);

		} else if(this.aboutTool.contains(event.target)){

			popupManager.openPopup("about",400,500);

		} else if(this.editTool.contains(event.target)){
			main.comparator.editText();
			
		} else if(this.compareTool.contains(event.target)){
			main.comparator.compareText();
		}
	}

	isOpen(){
		return this.open;
	}

	openSidebar() {
		this.open = true;
		this.sidebar.classList.remove("sidebar-collapse");
	}

	closeSidebar() {
		this.open = false;
		this.sidebar.classList.add("sidebar-collapse");
	}

	toggleSidebar() {
		this.open = !this.open;
		this.sidebar.classList.toggle("sidebar-collapse");
	}

	setToolActive(tool, active) {
		if(active)
			document.getElementById(tool+"-tool").classList.add("sidebar-tool-active");
		else
			document.getElementById(tool+"-tool").classList.remove("sidebar-tool-active");
	}

	setToolDisabled(tool, disabled) {
		if(disabled)
			document.getElementById(tool+"-tool").classList.add("sidebar-tool-disabled");
		else
			document.getElementById(tool+"-tool").classList.remove("sidebar-tool-disabled");
	}
}