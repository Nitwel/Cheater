class Dropdowns {

	constructor() {
		this.dropdowns = document.getElementsByClassName("dropdown");
	}

	clicked(event) {
		event.dropdownTarget.classList.toggle("display-dropdown");
	}
}