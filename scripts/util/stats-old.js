//die Ähnlicheit in abhängigkeit der anzahl der Buchstaben.
const wordDiff = 0;
//wie ähnlich die Sätze mindestens sein dürfen, damit sie verbunden werden können.
const sentenceMinEqual = 0;
//-------------------------[non constant variables]-------------------------//

//all combined sentences
var sentenceConnections = [];
//all combined words
var wordBounds = [];

onmessage = function(e) {
	calculateStats(e.data.data.text1, e.data.data.text2);
}

function calculateStats(text1, text2){

	if(text1 == "" || text2 == ""){
		return;
	}

	//reset stats
	sentenceConnections = [];
	wordBounds = [];

	var sentences1 = [];
	var sentences2 = [];

	if(!text1.endsWith("."))text1 += ".";
	if(!text2.endsWith("."))text2 += ".";

	var match1 = text1.match(/.+?\D+?[.!?\n]/g);
	var match2 = text2.match(/.+?\D+?[.!?\n]/g);

	for (var i = 0; i < match1.length; i++) {
		var sentence = match1[i].replace(/\.$/g,"").replace(/\s+/g," ").replace(/^\s/g,"").replace(/\s$/g,"");
		sentences1.push(sentence);
	}
	for (var i = 0; i < match2.length; i++) {
		var sentence = match2[i].replace(/\.$/g,"").replace(/\s+/g," ").replace(/^\s/g,"").replace(/\s$/g,"");
		sentences2.push(sentence);
	}

	//berechnen der Anzahl Sätze und Wörter
	var totalWords = 0;
	var sameSentences = 0;
	var sameWords = 0;

	console.log("1: " + sentences1.length + " 2: " + sentences2.length);

	var totalSentences = (sentences1.length + sentences2.length)/2;
	for (var i = 0; i < sentences1.length; i++) {
		totalWords += sentences1[i].split(" ").length;
	}
	for (var i = 0; i < sentences2.length; i++) {

		totalWords += sentences2[i].split(" ").length;
	}
	totalWords = totalWords/2;

	//console.log("satz1: "+ sentences1);
	//console.log("satz2: "+ sentences2);

	var conections = [];

	//das verbinden der Sätze beider Texte
	for (var i = 0; i < sentences1.length; i++) {

		var sentenceConnection = null;

		for (var h = 0; h < sentences2.length; h++) {

			if(!conections.includes(h)){
				var diff = Math.abs(i - h);

				//wenn die differenz der Positionen kleiner als 5 Sätze ist.
				if(diff < 5){
					//informationen zu den verglichenen Sätzen
					var comparedSentences = sentenceEquals(sentences1[i], sentences2[h]);
					var simularity = comparedSentences.simularity;

					//console.log("i: " + i + " h: " + h +  " equal " + Math.round(simularity*100)/100 + " diff: " + diff);

					if(simularity > 0.5){
						if(sentenceConnection != null &&
							compareValues(sentenceConnection.sentence.simularity, sentenceConnection.diff) < compareValues(simularity, diff)){

							sentenceConnection = new SentenceConnection(i, h, diff, comparedSentences);
						} else if(sentenceConnection == null){

							sentenceConnection = new SentenceConnection(i, h, diff, comparedSentences);
						}
					}
				}
			}
		}
		if(sentenceConnection != null){
			conections.push(h);
			sentenceConnections.push(sentenceConnection);
			sameWords += sentenceConnection.sentence.simularWords;
			sameSentences += sentenceConnection.sentence.simularity;
		}
		if(totalWords > 2000 && i % 25 == 0){
			self.postMessage({
				'status': i/sentences1.length
			});
		}

	}

	console.log(sentenceConnections);
	var sentenceSum = 0;
	for (var i = 0; i < sentenceConnections.length; i++) {
		//console.log("i: "+i+" sum: "+sentenceConnections[i][2]);
		sentenceSum += sentenceConnections[i].sentence.simularity;
	}
	//console.log("sum: "+sentenceSum+" se1: "+sentences1.length+" se2: "+sentences2.length);
	//console.log((sentenceSum / ((sentences1.length + sentences2.length)/2))*100);
	var colorSText1 = generateColoredText(sentences1, 1, false);
	var colorSText2 = generateColoredText(sentences2, 2, false);

	var colorWText1 = generateColoredText(sentences1, 1, true);
	var colorWText2 = generateColoredText(sentences2, 2, true);

	textEqualPercentage = (sentenceSum / totalSentences)*100;

	self.postMessage({
		"data": new TextData(textEqualPercentage, totalSentences, totalWords,
			sameSentences, sameWords, text1, text2, colorSText1, colorSText2, colorWText1, colorWText2)
	});
}

function sentenceEquals(sentence1, sentence2) {
	var words1 = sentence1.split(" ");
	var words2 = sentence2.split(" ");
	var simularWords = 0;
	//liste von allem Wörtern
	var connectedWords = [];
	var text2Connections = [];
	//console.log("words1: " + words1 + " words2: " + words2);

	for (var i = 0; i < words1.length; i++) {
		//das ähnlichste Wort
		var wordConnection = null;

		for (var h = 0; h < words2.length; h++) {

			if(!text2Connections.includes(h) && words1[i].replace(/ /g,"") != "" && words2[h].replace(/ /g,"") != ""){

				var equal = wordsEquals(words1[i], words2[h]);
				var diff = Math.abs(i - h)/2;

				//console.log(words1[i] + " | " + words2[h] + " equal: " + equal + " diff: " + diff);

				if(equal >= wordDiff){

					if(wordConnection != null && compareValues(wordConnection.simularity, wordConnection.diff) < compareValues(equal, diff)){

						wordConnection = new WordConnection(i, h, equal, diff);
						//console.log("now use: " + words1[i] + " | " + words2[h]);

					} else if(wordConnection == null){

						wordConnection = new WordConnection(i, h, equal, diff);

					}
				}
			}
		}
		if(wordConnection != null){
			//console.log("Same ==> " + words1[wordConnection.xPos] + " | " + words2[wordConnection.yPos] + " Sameness: " + wordConnection.simularity);
			text2Connections.push(wordConnection.yPos);
			simularWords += wordConnection.simularity;
			connectedWords.push(wordConnection);
		}
	}
	var wordLength = Math.max(words1.length, words2.length);
	var simularity = simularWords/wordLength;

	//console.log("sameWords: " + simularWords +  " / " + (words1.length + words2.length)/2 );
	return new Sentence(simularity, wordLength, simularWords, connectedWords);
}

function wordsEquals(word1, word2) {
	if(word1.toLowerCase() == word2.toLowerCase()){
		return 1;
	}
	//aufteilen der Wörter in Buchstaben
	chars1 = word1.toLowerCase().split("");
	chars2 = word2.toLowerCase().split("");

	var lengthChars = Math.max(chars1.length, chars2.length);
	var connections = [];
	var simularity = 0;

	for (var i = 0; i < word1.length; i++) {

		var charConnection = null;

		for (var h = 0; h < word2.length; h++) {

			if(!connections.includes(h)){

				var diff = Math.abs(i - h);

				if(charConnection != null && word1[i] == word2[h] && charConnection[1] > diff){

					charConnection = [h, diff];
					//console.log(chars1[i] + " | " + chars2[h]);

				} else if(charConnection == null && word1[i] == word2[h]){

					charConnection = [h, diff];

				}
			}
		}

		if(charConnection != null){
			connections.push(charConnection[0]);
			simularity += 1/Math.sqrt(charConnection[1]+1);
		}
	}
	return simularity / lengthChars;
}

function compareValues(simularity, diff) {
	return Math.pow(simularity, (diff+11)/10);
}

function TextData(simularity, totalSentences, totalWords, sameSentences, sameWords,
	defaultText1, defaultText2, colorSText1, colorSText2, colorWText1, colorWText2) {
	this.simularity = simularity;
	this.totalSentences = totalSentences;
	this.totalWords = totalWords;
	this.sameSentences = sameSentences;
	this.sameWords = sameWords;
	this.defaultText1 = defaultText1;
	this.defaultText2 = defaultText2;
	this.colorSText1 = colorSText1;
	this.colorSText2 = colorSText2;
	this.colorWText1 = colorWText1;
	this.colorWText2 = colorWText2;
}

function SentenceConnection(text1Pos, text2Pos, diff, sentence) {
	this.xPos = text1Pos;
	this.yPos = text2Pos;
	this.diff = diff;
	this.sentence = sentence;
}

function Sentence(simularity, wordLength, simularWords, wordConnections){
	this.simularity = simularity;
	this.length = wordLength;
	this.simularWords = simularWords;
	this.wordConn = wordConnections;
}

function WordConnection(sentencePos1, sentencePos2, simularity, diff) {
	this.xPos = sentencePos1;
	this.yPos = sentencePos2;
	this.simularity = simularity;
	this.diff = diff;
}

function removeColoredText(text) {
	var regex = /(<\/?span ?.*?>)/g;
	return text.replace(regex,"");
}

function generateColoredText(sentences, id, wordsSensitive){
	var text1 = "";
	for (var i = 0; i < sentences.length; i++) {
		sentences[i] = sentences[i].replace(/\n/g,"<br>");
		var found = null;

		for (var v = 0; v < sentenceConnections.length && found==null; v++) {
			if(id == 1){
				if(sentenceConnections[v].xPos == i){
					found = v;
				}
			} else {
				if(sentenceConnections[v].yPos == i){
					found = v;
				}
			}

		}
		if(found != null){

			var simular = Math.round(sentenceConnections[found].sentence.simularity*10000)/100;

			if(wordsSensitive){

				var words = sentences[i].split(" ");
				var coloredSentence = "";
				var wordCombinations = sentenceConnections[found].sentence.wordConn;

				for (var w = 0; w < words.length; w++) {

					var wFound = null;

					for(var c = 0; c < wordCombinations.length && wFound == null; c++){

						if(id == 1){
							if(wordCombinations[c].xPos == w){
								wFound = c;
							}
						} else {
							if(wordCombinations[c].yPos == w){
								wFound = c;
							}
						}

					}
					if(wFound != null){
						var wordSimular = Math.round(wordCombinations[wFound].simularity*10000)/100;
						if(w == words.length-1){
							coloredSentence += "<span id='" + id + "." + found + "." + wFound + "' class='text-marked hover' hover='Ähnlicheit: " +
							wordSimular + "%'>" + words[w] + "</span>";
						} else {
							coloredSentence += "<span id='" + id + "." + found + "." + wFound + "' class='text-marked hover' hover='Ähnlicheit: " +
							wordSimular + "%'>" + words[w] + " </span>";
						}
					} else {
						if(w == words.length-1){
							coloredSentence += "<span>" + words[w] + "</span>";
						} else {
							coloredSentence += "<span>" + words[w] + " </span>";
						}

					}


				}
				text1 += coloredSentence + ". ";
				//text1 += "<span id='" + id + "." + found + "' class='text-marked' title='Ähnlicheit: " + simular + "%'>" + coloredSentence + ". </span>";
			} else {
				text1 += "<span id='" + id + "." + found + "' class='text-marked hover' hover='Ähnlicheit: " + simular + "%'>" + sentences[i] + ". </span>";
			}


		} else {
			text1 += sentences[i]+ ". ";
		}
	}
	return text1;
}
