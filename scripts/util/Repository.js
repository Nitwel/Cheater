class Repository {

	constructor() {

	}

	static getInstance(){
		if(this.repository == null) {
			this.repository = new Repository();
		}
		return this.repository;
	}

	hasParentWithClass(element, className) {
		if(element == document)return false;
		if(element.classList.contains(className))return true;
		return this.hasParentWithClass(element.parentNode, className);
	}

	getParentWithClass(element, className) {
		if(element == document)return null;
			if(element.classList.contains(className))return element;
			return this.getParentWithClass(element.parentNode, className);
	}

	toPercent(n){
		return (Math.round(n*100));
	}

	roundInt(value, length) {
		return Math.round(value*Math.pow(10,length))/Math.pow(10,length);
	}
}