class RippleEffect {

	constructor() {
		this.repo = Repository.getInstance();
		this.counter = 0;
		document.addEventListener("click", (event) => {this.playRipple(event)});
	}

	playRipple(event) {
		var rippleParent = this.repo.getParentWithClass(event.target, "ripple");
		if(rippleParent == null)return;
		var ripple = document.createElement("div");
		ripple.id = "ripple";
		ripple.classList.add("ripple-animation");
		rippleParent.appendChild(ripple);

		var targetRect = rippleParent.getBoundingClientRect();
		var size = Math.max(targetRect.width, targetRect.height);
		ripple.style.width = size+"px";
		ripple.style.height = size+"px";
		
		ripple.style.left = event.pageX - size/2 - targetRect.left+"px";
		ripple.style.top = event.pageY - size/2 - targetRect.top + "px";
		setTimeout(function() {
			rippleParent.removeChild(ripple);
		}, 550);
		this.counter++;
	}

}